package com.wahyu18090108.retrofitlatihan

import retrofit2.Call
import retrofit2.http.GET

interface Api{
    @GET("posts")
    fun getPosts(): Call<ArrayList<PostResponse>>
}
